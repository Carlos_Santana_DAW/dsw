<?php
class NotFoundException extends exception

{
    public function __construct (string $message) {

        parent::__construct($message);

    }
}
?>