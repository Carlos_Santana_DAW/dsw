<?php

class File
{
    private $file;

    private $fileName;


    public function __construct(string $fileName, array $arrTypes)
    {

        $this->file = $_FILES[$fileName];

        $this->fileName = "";

        if (($this->file["name"] == "")) {


            throw new FileException("Debes especificar un fichero", 1);
        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {

            switch ($this->file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:

                    //TO-DO: Error de tamaño
                    throw new FileException("Debes poner un archivo de el un tamaño correcto", 1);

                case UPLOAD_ERR_PARTIAL:

                    //TO-DO: Error de archivo incompleto
                    throw new FileException("El archivo esta incompleto", 1);

                default:

                    // TO-DO: error genérico en la subida del fichero
                    throw new FileException("Error a la hora de la subida del archivo", 1);

                    break;
            }
        }

        if (in_array($this->file["type"], $arrTypes) === false) {

            //TO-DO error de tipo
            throw new FileException("El tipo no es el correcto", 1);

        }
    }

    public function saveUploadFile( $ruta)
    {

        

        if(is_uploaded_file($this->file["tmp_name"])==false){
            throw new FileException("El archivo no se ha subido mediante un formulario", 1);
        }

        if(is_file($this->file["tmp_name"])==false){
            $idUnico = time()."_";
            $this->file["name"] = $idUnico.$this->file["name"];
        }

        if (move_uploaded_file ( $this->file["tmp_name"] ,$ruta.$this->file["name"] ) ==false){
            throw new FileException("No se ha podido mover el fichero al destino especificado", 1);
        }else{
            return array (

            "nombre"=>$this->file["name"]
            );
        }

        return null;

    }

    public function copyFile($origen, $destino){
       $origen .= $this->file["name"];
       $destino .=  $this->file["name"];

        if (is_file($origen) == false){
            
            throw new FileException("No existe el fichero $origen", 1);
        }

        if (is_file(($destino))){
            throw new FileException("El fichero $destino ya existe", 1);
        }

        if (copy($origen, $destino) == false){
            
            throw new FileException("No se ha podido copiar el fichero", 1);
        }
    }


    /**
     * Get the value of fileName
     */
    public function getFileName()
    {
        return $this->fileName;
    }


    public function getName(){
        return $this->file["name"];
    }
}
?>