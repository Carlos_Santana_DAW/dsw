<?php

class ImagenGaleria
{
    const Ruta_Imagenes_Portfolio ="images/index/portfolio/";
    const Ruta_Imagenes_Galeria ="images/index/gallery/";
    
    private $nombre;

    private $descripcion;

    private $categoria;

    private $numVisualizaciones;

    private $numLikes;

    private $numDownloads;

    

public function __construct($nombre="",$categoria=0,$descripcion="",$numVisualizaciones=0,$numLikes=0,$numDownloads=0)
    {
       $this->nombre = $nombre;
       $this->categoria = $categoria;
       $this->descripcion= $descripcion;
       $this->numVisualizaciones = $numVisualizaciones;
       $this->numLikes = $numLikes;
       $this->numDownloads = $numDownloads; 
    }
  

//getters and setters

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get the value of numVisualizaciones
     */ 
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
     * Set the value of numVisualizaciones
     *
     * @return  self
     */ 
    public function setNumVisualizaciones($numVisualizaciones)
    {
        $this->numVisualizaciones = $numVisualizaciones;

        return $this;
    }

    /**
     * Get the value of numLikes
     */ 
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
     * Set the value of numLikes
     *
     * @return  self
     */ 
    public function setNumLikes($numLikes)
    {
        $this->numLikes = $numLikes;

        return $this;
    }

    /**
     * Get the value of numDownloads
     */ 
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    /**
     * Set the value of numDownloads
     *
     * @return  self
     */ 
    public function setNumDownloads($numDownloads)
    {
        $this->numDownloads = $numDownloads;

        return $this;
    }
    
    /**
     * Get the value of categoria
     */ 
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of categoria
     *
     * @return  self
     */ 
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }
    //funcion geturlportfolio
    public function getURLPortfolio() : string {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
      }
      //funcion getGallery
      public function getURLGallery() : string {
        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
      }

      public function toArray(): array
      {
          return [
  
              "nombre"=>$this->getNombre(),
              
              "categoria"=>$this->getCategoria(),
  
              "descripcion"=>$this->getDescripcion(),
  
              "numVisualizaciones"=>$this->getNumVisualizaciones(),
             
              "numLikes"=>$this->getNumLikes(),
             
              "numDownloads"=>$this->getNumDownloads()
          ];
        }
}
?>