<?php
//Declaracion de variables 
$nombre = htmlentities($_POST['nombre'] ?? null);
$apellido = htmlentities($_POST['apellido'] ?? null);
$email = htmlentities($_POST['email'] ?? null);
$subject = htmlentities($_POST['subject'] ?? null);
 // Arrays para guardar mensajes y errores:
 $Errores = array();

 // Comprobar si se ha enviado el formulario:
 if( !empty($_POST) )
 {
     echo "FORMULARIO RECIBIDO:<br/>";

     // Comprobar si llegaron los campos requeridos:
     if( isset($nombre) )
     {
         echo($nombre);
     
     } else {
         echo("Nombre no encontrado");
         array_push($Errores);
     }
     if( filter_var($email,FILTER_VALIDATE_EMAIL) )
     {
         echo($email);
     
     } else {
         echo("Email incorrecto");
         array_push($Errores);
     }
     if( isset($subject) )
     {
         echo($subject);
     
     } else {
         echo("Asignatura incorrecta");
         array_push($Errores);
     }
      
    } else {
        echo("formulario no recibido");
    }
// Si han habido errores se muestran, sino se mostrán los mensajes
     //if( count($aErrores) > 0 )
     //{
       //  echo "<p>ERRORES ENCONTRADOS:</p>";

         // Mostrar los errores:
         //for( $contador=0; $contador < count($aErrores); $contador++ )
           //  echo $aErrores[$contador]."<br/>";

           require "views/contact.view.php";
           require "util/utils.php";

?>