<?php
require "entity/File.php";
require "entity/ImagenGaleria.php";
require "utils/utils.php";
require "views/galeria.view.php";



if ($_SERVER["REQUEST_METHOD"]==="POST") {

    try {

        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));

        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

        $imagen = new File("imagen", $tiposAceptados);       

        $mensaje = "Datos enviados";

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALERIA);

        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALERIA, ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

    } catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }   

}

?>