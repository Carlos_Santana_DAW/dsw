<?php
//Declaracion de variables 
$nombre = htmlentities($_POST['nombre'] ?? null);
$apellido = htmlentities($_POST['apellido'] ?? null);
$email = htmlentities($_POST['email'] ?? null);
$subject = htmlentities($_POST['titulo'] ?? null);
 // Arrays para guardar mensajes y errores:
 $Errores = array();

 // Comprobar si se ha enviado el formulario:
    if (isset($nombre) && isset($email) && isset($subject)) {

        if (strlen($nombre) == 0 && strlen($email) == 0 && strlen($subject) == 0) {
            array_push($errores, "Faltan todos los parametros");
        } else if (strlen($_nombre) == 0) {
            array_push($errores, "Falta escribir el nombre");
        } else if (strlen($_email) == 0) {
            array_push($errores, "Falta escribir el correo");
        } else if (strlen($_subject) == 0) {
            array_push($errores, "Falta escribir un titulo");
        } else if (strpos($_email, "@") == false) {
            array_push($errores, "El correo esta mal escrito");
        }
    
    
        require __DIR__ . "/../views/contact.view.php";
    } else {
        $_POST["nombre"] = '  ';
        $_POST["apellidos"] = '  ';
        $_POST["correo"] = '  ';
        $_POST["titulo"] = '  ';
        $_POST["mensaje"] = '  ';
    
        require __DIR__ . "/../views/contact.view.php";
    }
    
// Si han habido errores se muestran, sino se mostrán los mensajes
     //if( count($aErrores) > 0 )
     //{
       //  echo "<p>ERRORES ENCONTRADOS:</p>";

         // Mostrar los errores:
         //for( $contador=0; $contador < count($aErrores); $contador++ )
           //  echo $aErrores[$contador]."<br/>";

?>