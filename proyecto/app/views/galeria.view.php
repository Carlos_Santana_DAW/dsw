<?php
include __DIR__ . "/partials/inicio-doc.part.php";

include __DIR__ . "/partials/nav.part.php";

?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <!-- Aqui es donde metemos la comprobacion si existe un metodo post y si es asi realizamos otra comprobacion en la alerta si hay errores en el array si es el caso lo imprimimos y en caso de que no imprimimos el mensaje
            del tryCatch -->
            <?php if (empty($errores) == false) : ?>
                <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                    <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <!-- Aqui acemos otra comprobacion si existe algun error en la variable, si el caso es que no realizamos un mensaje que lo definimos en galeria.php en el tryCatch y en el caso de 
            que si exista algun error se realizara un foreach del array completo  -->
                    <?php if (empty($errores)) : ?>
                        <p><?= $mensaje ?></p>
                    <?php else : ?>
                        <ul>
                            <?php foreach ($errores as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <form class="form-horizontal" action="#" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="form-group col-xs-12">
                    <label class="label-control">Categoría</label>
                    <select class="form-control" name="categoria">
                        <?php foreach ($categorias as $categoria) : ?>
                            <option value="<?= $categoria->getId() ?>" 
                            
                            <?= $categoria == $categoria->getId() ? 'selected' : '' ?>>

                            <?= $categoria->getNombre(); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= isset($descripcion) ? $descripcion : "" ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>



            </form>


            <!-- Tabla de las i$imagen -->
            <table style="margin-top: 10px;" border="1">
                <tr>
                    <th style="padding: 6px;">Id</th>
                    <th style="padding: 6px;">Nombre</th>
                    <th style="padding: 6px;">Categoría</th>
                    <th style="padding: 6px;">Descripción</th>
                    <th style="padding: 6px;">numVisualizacion</th>
                    <th style="padding: 6px;">numLikes</th>
                    <th style="padding: 6px;">numDownload</th>
                </tr>

                <?php

                if (empty($imagenes) == false) {


                    foreach ($imagenes as $imagen) {



                ?>
                        <tr>
                            <td style="text-aling:center; padding: 6px;"><?= $imagen->getId()  ?></td>
                            <td style="text-aling:center; padding: 6px;"><?= $imagen->getNombre()  ?></td>
                            <td style="text-aling:center; padding: 6px;"><?= $imagenGaleriaRepository->getCategoria($imagen)->getNombre() ?></td>
                            <td style="text-aling:center; padding: 6px;"><?= $imagen->getDescripcion()  ?></td>
                            <td style="text-aling:center; padding: 6px;"><?= $imagen->getNumVisualizaciones()  ?></td>
                            <td style="text-aling:center; padding: 6px;"><?= $imagen->getNumLikes()  ?></td>
                            <td style="text-aling:center; padding: 6px;"><?= $imagen->getNumDownloads()  ?></td>



                        </tr>
                <?php
                    }
                }
                ?>
            </table>


        </div>
    </div>
</div>

<?php include __DIR__ . "/partials/fin-doc.part.php"; ?>