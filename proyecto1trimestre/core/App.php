<?php

class App
{

    //Almacena los datos en nuestro contenedor

    private static $container = [];

    // Esta funcion nos sirve como setter de los valores de la conexion,

    public static function bind(string $key, $value)

    {
        static::$container[$key] = $value;
    }


    // Con esta conseguiremos las configuraciones del objeto PDO
    public static function get(string $key)

    {

        if (!array_key_exists($key, static::$container))

            throw new AppException("No se ha encontrado la clave $key en el contenedor.");


        return static::$container[$key];
    }

    // Y esta ultima sera el getter de la conexion que la guardaremos en la variable conection.
    public static function getConnection()

    {

        if (!array_key_exists("connection", static::$container))

            static::$container["connection"] = Conection::make();

        return static::$container["connection"];
    }
}


?>
