<?php 

//Archivo donde guardaremos las configuraciones usadas

require_once "App.php";
require_once "utils/MyLog.php";
require_once "Request.php";
require_once "Exceptions/FileException.php";
require_once "Exceptions/QueryException.php";
require_once "Exceptions/AppException.php";
require_once "Exceptions/NotFoundException.php";
require_once "database/QueryBuilder.php";
require_once "core/App.php";
require_once "database/IEntity.php";
require_once "entity/Task.php";
require_once "entity/Usuario.php";
require_once "database/Conection.php";
require_once "Repository/TaskRepository.php";
require_once "Repository/UsuarioRepository.php";
require_once "vendor/autoload.php";




//Configuracion para los logs

$logger = MyLog::load("logs/info.log");

App::bind("logger", $logger);

//Configuracion para la base de datos

$config = require_once __DIR__ . "/../app/config.php";

App::bind("config", $config);

?>