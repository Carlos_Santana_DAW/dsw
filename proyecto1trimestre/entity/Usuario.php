<?php


//Clase Usuario con propiedades id , nombre y password
class Usuario implements IEntity
{

    private $id;

    private $nombre;

    private $password;

    public function __construct($id = 0, $nombre = "", $password="")

    {

        $this->id = $id;

        $this->nombre = $nombre;

        $this->password = $password;

    }


// Sus respectivos getters

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }


    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }




// Y la funcion toArray que mete sus propiedades en un array
    public function toArray(): array
    {
        return [
            "id" => $this->getId(),

            "nombre" => $this->getNombre(),

            "password"=> $this->getPassword()
        ];
    }

    
}