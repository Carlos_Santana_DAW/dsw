<?php
//Aqui tenemos los espacios de nombres de monolog que usamos gracias al autoload
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


//Y esta clase sirve para los logs que crearemos con el monoglog
class MyLog

{

    private $log;



    private function __construct(string $filename)
    {

        $this->log = new Logger('proyecto');

        $this->log->pushHandler(new StreamHandler($filename, Logger::INFO));
    }



    public static function load(string $filename)
    {

        return new MyLog($filename);
    }



    public function add(string $message)
    {

        $this->log->info($message);
    }
}

?>