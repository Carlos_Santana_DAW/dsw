<!-- Partial Head -->
<?php include __DIR__ . "/partials/head-doc.part.php"; ?>

<body>
    <!-- Partial nav -->

    <?php include __DIR__ . "/partials/nav-doc.part.php"; ?>


    <!-- banner part start-->
    <section class="banner_part">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-xl-6">
                    <div class="banner_text">
                        <div class="banner_text_iner">
                            <h5>Agenda de Tareas</h5>
                            <h1>Aqui saldrán las tareas que tengas pendientes</h1>
                            <p>Si, una agenda personal online para no olvidarnos y no suspender las asignaturas</p>
                            <a href="task" class="btn_1">Mirar tus tareas pendientes </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- banner part start-->

    <!-- feature_part start-->
    <section class="feature_part mb_110">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xl-3 align-self-center">
                    <div class="single_feature_text ">
                        <h2>Crea tu <br> Tarea</h2>
                        <p>Prueba a crear tu primer recordatorio</p>
                        <a href="#" class="btn_1">Read More</a>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-layers"></i></span>
                            <h4>Seleccionar</h4>
                            <p>Elige cual de las que tienes es mas importante y realizala</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><i class="ti-new-window"></i></span>
                            <h4>Empieza ahora</h4>
                            <p>Completamente gratis, eso si, si añades tareas, hazlas ¬¬</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part single_feature_part_2">
                            <span class="single_service_icon style_icon"><i class="ti-light-bulb"></i></span>
                            <h4>Listo</h4>
                            <p>Si completas la tarea, solo marcala como realizada</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- upcoming_event part start-->

    <!-- member_counter counter start -->
    <section class="member_counter">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1024</span>
                        <h4>Tareas Realizadas</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">960</span>
                        <h4>Tareas pendientes</h4>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="single_member_counter">
                        <span class="counter">1018</span>
                        <h4>Creadores contentos</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- member_counter counter end -->

    <!--::blog_part start::-->
    <section id="Trabajos" class="blog_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div  class="section_tittle text-center">
                        <h2>Trabajos por hacer</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <?php
                if (empty($tareas) == false) {


                    foreach ($tareas as $tarea) {



                ?>
                        <div class="card" style="width:700px; margin-bottom: 60px; border: 2px solid black;">
                        
                            <div class="card-body">
                                <h2 class="card-title"><?=$tarea->getNombre()?></h2>
                                <h5 class=""><strong>Correo:</strong> <?=$tarea->getEmail()?></h5>
                                <h5 class=""><strong>Telefono:</strong> <?=$tarea->getTelefono()?></h5>
                                 <p class="card-text"><?= $tarea->getRequerimientos()  ?></p>                            </div>
                            <button class="btn_3">Envia un recordatorio a tu correo</button>
                        </div>
                <?php
                    }
                }
                ?>
            </div>
        </div>
    </section>
    <!--::blog_part end::-->

    <!-- Partial Footer -->

    <?php include __DIR__ . "/partials/footer-doc.part.php"; ?>