<!--Partial Head -->

<?php include __DIR__ . "/partials/head-doc.part.php"; ?>

<body>
    <!-- Partial nav -->

    <?php include __DIR__ . "/partials/nav-doc.part.php"; ?>

    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2>Contact us</h2>
                            <p>Home<span>/<span>Contact us</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->


    <section class="blog_part section_padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <h2>Trabajamos para mejorar nuestro proyecto</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!-- Partial Footer -->

<?php include __DIR__ . "/partials/footer-doc.part.php"; ?>