<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="css/login.css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="http://danielzawadzki.com/codepen/01/icon.svg" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form action="" method="POST">
      <input type="text" id="login" class="fadeIn second" name="login_name" placeholder="login">
      <input type="password" id="password" class="fadeIn third" name="login_pass" placeholder="password">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>

    <div class="mt-10 text-center">
					<?php if (empty($errores) == false) : ?>
						<div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
							<button type="button" class="clase" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">x</span>
							</button>
							<!-- Aqui acemos otra comprobacion si existe algun error en la variable, si el caso es que no realizamos un mensaje que lo definimos en galeria.php en el tryCatch y en el caso de 
								que si exista algun error se realizara un foreach del array completo  -->
							<ul>
								<?php foreach ($errores as $error) : ?>
									<li><?= $error ?></li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php else : ?>
						<p><?= isset($mensaje) ? $mensaje : "" ?></p>
					<?php
					endif;
					?>
			</div>

  </div>
</div>