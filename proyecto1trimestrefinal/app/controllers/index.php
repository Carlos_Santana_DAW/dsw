<?php
//fichero donde tenemos todo lo imprescindible para la aplicacion
require_once "core/bootstrap.php";

//Creacion de un tryCatch para hacer el findAll de las tareas y si no hay ningun registro que capturemos la excepcion.
try{
    $taskRepository = new TaskRepository();
    $tareas = $taskRepository->findAll();
} catch (NotFoundException $notFoundException){
    $errores[] = $notFoundException->getMessage();
}

// Vista del index
if(!isset($_SESSION["account"])){
    header("Location: http://192.168.0.149/front-end/proyecto1trimestrefinal/login");
}

require __DIR__ . "/../views/index.view.php";

?>

