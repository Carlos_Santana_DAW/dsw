<?php
//Creacion de un array de errores para los errores de validacion
$errores = [];


try {
    
    // Conseguimos del contenedor la conexion con un getter
    $connection = App::getConnection();

    //Instanciamos los idiomas y las tareas con sus repositorios
    $taskRepository = new TaskRepository();

    //Validamos si existe el metodo post y es cuando hacemos las validaciones correspondientes
    if ($_SERVER["REQUEST_METHOD"] === "POST") {

        if (
            empty($_POST["nombre"]) && empty($_POST["email"]) && empty($_POST["telefono"]) && empty($_POST["requerimientos"])
        ) {
            array_push($errores, "No hay ningun parametro en el formulario");
        } else if (empty($_POST["nombre"])) {
            array_push($errores, "No hay ningun parametro en nombre");
        } else if (empty($_POST["requerimientos"])) {
            array_push($errores, "No hay ningun objetivo");
        } else if (empty($_POST["email"])) {
            array_push($errores, "No hay ningun correo");
        } else if ((strpos($_POST["email"], "@") == false)) {
            array_push($errores, "El correo esta mal escrito");
        } else {

            //Aqui evitamos la inyeccion de html y la inyeccion sql
            $nombre = trim(htmlspecialchars($_POST["nombre"]));
            $requerimientos = trim(htmlspecialchars($_POST["requerimientos"]));
            $telefono = trim(htmlspecialchars($_POST["telefono"]));
            $email = $_POST["email"];
            //Instanciamos un nuevo objeto tarea
            $task = new Task($nombre, $email, $telefono, $requerimientos);

            //Con la funcion Save de el QueryBuilder guardamos el objeto en la base de datos 
            $taskRepository->save($task);

            //Creamos el mensaje de respuesta
            $mensaje = "la información ha sido enviada correctamente";

            App::get("logger")->add($mensaje);

            //Y redireccionamos a la pagina principal donde se mostraran las tareas
            header("Location: http://192.168.0.149/front-end/proyecto1trimestrefinal/index");
        }
    }
    //Capturamos las excepciones
} catch (FileException $fileException) {

    $errores[] = $fileException->getMessage();
} catch (AppException $appException) {

    $errores[] = $appException->getMessage();
} catch (NotFoundException $notFoundException){

}

//Vista para la creacion de tareas

if(!isset($_SESSION["account"])){
    header("Location: http://192.168.0.149/front-end/proyecto1trimestrefinal/login");
}

require __DIR__ . "/../views/task.view.php";
