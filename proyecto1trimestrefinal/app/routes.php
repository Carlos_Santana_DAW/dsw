<?php 
//Fichero donde se guardan las rutas a la páginas
return [

 "front-end/proyecto1trimestrefinal/index" => "app/controllers/index.php",

 "front-end/proyecto1trimestrefinal/about" => "app/controllers/about.php",

 "front-end/proyecto1trimestrefinal/blog" => "app/controllers/blog.php",

 "front-end/proyecto1trimestrefinal/single-blog" => "app/controllers/single-blog.php",

 "front-end/proyecto1trimestrefinal/elements" => "app/controllers/elements.php",

 "front-end/proyecto1trimestrefinal/contact" => "app/controllers/contact.php",

 "front-end/proyecto1trimestrefinal/task" => "app/controllers/task.php",

 "front-end/proyecto1trimestrefinal/login" => "app/controllers/login.php",

 "front-end/proyecto1trimestrefinal/logOut" => "app/controllers/logOut.php",
]

?>