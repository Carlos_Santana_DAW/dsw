<?php
//Fichero donde se guarda la configuracion de la base de datos y sus credenciales
return [

    "database" => [

        "name" => "proyecto",

        "username" => "carlos",

        "password" => "1234",

        "connection" => "mysql:host=localhost",

        "options" => [

            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,

            PDO::ATTR_PERSISTENT => true

        ]

    ]

];

?>