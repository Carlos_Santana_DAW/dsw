<?php


//Clase tarea con sus propiedades que se le pasaran por el formulario a la hora de crear dicho objeto

class Task implements IEntity
{

    private $id;

    private $nombre;

    private $email;

    private $telefono;

    private $requerimientos;

    private $pago;


    public function __construct($nombre="", $email="", $idioma=0, $telefono="", $requerimientos="")
    {
        $this->id = null;

        $this->nombre = $nombre;

        $this->email = $email;
        
        $this->telefono = $telefono;

        $this->requerimientos = $requerimientos;

    

    }


// Los getters correspondientes

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    
    /**
     * Get the value of telefono
     */ 
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Get the value of requerimientos
     */ 
    public function getRequerimientos()
    {
        return $this->requerimientos;
    }

    /**
     * Get the value of pago
     */ 
    public function getPago()
    {
        return $this->pago;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    public function toArray(): array
    {
        return [
            "id"=>$this->getId(),

            "nombre"=>$this->getNombre(),
            
            "email"=>$this->getEmail(),

            "telefono"=>$this->getTelefono(),
           
            "requerimientos"=>$this->getRequerimientos(),
           
            
        ];
    }

    
}

?>