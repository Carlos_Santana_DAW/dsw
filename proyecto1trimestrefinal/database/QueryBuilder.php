<?php


//La clase abstracta QueryBuilder que nos ayudara para evitar las inyecciones de sql y su uso para simplificar codigo
abstract class QueryBuilder
{
    private $connection;
    private $table;
    private $classEntity;


    public function __construct(string $table, string $classEntity)
    {

        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }


    //Esta funcion se usa para ejecutar cualquier script sql que se le pase por parametro.
    public function executeQuery(string $sql): array

    {

        $pdoStatement = $this->connection->prepare($sql);

        if ($pdoStatement->execute() === false){

            throw new QueryException("No se ha podido ejecutar la consulta");
        }
        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
    }


    //Esta funcion nos hace una consulta para ver todos los registros de dicha tabla.
    public function findAll()

    {
        $sql = "SELECT * FROM $this->table";
        $result = $this->executeQuery($sql);
        if(empty($result)){
            throw new NotFoundException("No hay registros");
        }

        return $result;
    }

    //Esta funcion la usamos para poder insertar datos en la tabla datos recogidos en forma de array de la clase que le pongamos por parametros
    public function save(IEntity $entity): void

    {

        try {

            $parameters = $entity->toArray();

            $sql = sprintf(
                "insert into %s (%s) values (%s)",

                $this->table,

                implode(", ", array_keys($parameters)),

                ":" . implode(", :", array_keys($parameters))

            );

            $statement = $this->connection->prepare($sql);

            $statement->execute($parameters);
        } catch (PDOException $exception) {

            throw new QueryException("Error al insertar en la BBDD.");
        }
    }


    //Y esta funcion es igual que la de findAll pero solo para encontrar un registro en concreto mediante su id.
    public function find(int $id): IEntity

    {

        $sql = "SELECT * FROM $this->table WHERE id=$id";

        $result = $this->executeQuery($sql);

        if (empty($result))

            throw new NotFoundException("No se ha encontrado el elemento con id $id");



        return $result[0];
    }

    //Y esta funcion es igual que la de find pero solo para encontrar un registro en concreto mediante el nombre.
    public function findByName(string $nombre): IEntity

    {

        $sql = "SELECT * FROM $this->table WHERE nombre='$nombre'";

        $result = $this->executeQuery($sql);

        if (empty($result))

            throw new NotFoundException("No se a encontrado a un usuario con ese nombre = $nombre");

        return $result[0];
    }
}
