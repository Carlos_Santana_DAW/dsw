<?php

//Esta interfaz la usaremos en cada clase creada con la funcion toArray para convertir las propiedades de las clases en un array.
interface IEntity
{

    public function toArray(): array;
}
?>