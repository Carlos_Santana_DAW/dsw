<?php

require_once "core/App.php";

//Esta clase nos sirve para crear la conexion a la base de datos con las configuraciones del fichero config y las funciones del fichero App

class Conection
{


    public static function make()
    {
        try {

            $config = App::get("config")["database"];

            $connection = new PDO(

                $config["connection"] . ";dbname=" . $config["name"],

                $config["username"],

                $config["password"],

                $config["options"]
            );
        } catch (PDOException $PDOException) {

            throw new AppException("No se ha podido conectar con la BBDD");
        }

        return $connection;
    }
}

?>