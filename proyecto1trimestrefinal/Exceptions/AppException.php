<?php

//Excepciones para un error de conexion de base de datos 
class AppException extends Exception

{

    public function __construct (string $message) {

        parent::__construct($message);

    }

}
?>