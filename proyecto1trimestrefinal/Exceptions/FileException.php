<?php

//Excepciones para un error en la subida de archivos
class FileException extends Exception

{

    public function __construct (string $message) {

        parent::__construct($message);

    }

}
?>
