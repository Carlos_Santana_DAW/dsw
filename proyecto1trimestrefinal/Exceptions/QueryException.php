<?php

//Excepciones para errores a la hora de realizar scrips sql 
class QueryException extends Exception

{

    public function __construct (string $message) {

        parent::__construct($message);

    }

}
?>